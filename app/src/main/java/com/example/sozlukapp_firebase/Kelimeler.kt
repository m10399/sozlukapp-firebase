package com.example.sozlukapp_firebase

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable
@IgnoreExtraProperties
data class Kelimeler(
    var Kelime_id:String = "",
    var Ingilizce:String = "",
    var Turkce:String = ""):Serializable {
}