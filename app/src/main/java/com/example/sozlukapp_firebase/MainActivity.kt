package com.example.sozlukapp_firebase

import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import com.example.sozlukapp_firebase.databinding.ActivityMainBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MainActivity : AppCompatActivity(),SearchView.OnQueryTextListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var kelimelerListe:ArrayList<Kelimeler>
    private lateinit var adapter:KelimeAdapter
    private lateinit var refKelimeler:DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbar.title = "Sözlük Uygulaması"
        setSupportActionBar(binding.toolbar)

        binding.rv.setHasFixedSize(true)
        binding.rv.layoutManager = LinearLayoutManager(this)

        val db = FirebaseDatabase.getInstance()
        refKelimeler = db.getReference("kelimeler")

        kelimelerListe = ArrayList()

        adapter = KelimeAdapter(this,kelimelerListe)
        binding.rv.adapter = adapter
        tumKelimeler()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu,menu)
        val item = menu?.findItem(R.id.action_ara)
        val searchView = item?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        aramaYap(query!!)
        Log.e("Kelimenin tamamı",query.toString())
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        aramaYap(newText!!)
        Log.e("Harf girdikçe",newText.toString())
        return true
    }
    fun tumKelimeler(){
        refKelimeler.addValueEventListener(object:ValueEventListener{
            override fun onDataChange(d: DataSnapshot) {
                kelimelerListe.clear()
               for (c in d.children){
                   val kelime = c.getValue(Kelimeler::class.java)
                   if (kelime != null){
                       kelime.Kelime_id = c.key!!
                       kelimelerListe.add(kelime)

                   }
               }
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }
    fun aramaYap(aramaKelime:String){
        refKelimeler.addValueEventListener(object:ValueEventListener{
            override fun onDataChange(d: DataSnapshot) {
                kelimelerListe.clear()
                for (c in d.children){
                    val kelime = c.getValue(Kelimeler::class.java)
                    if (kelime != null){
                        if (kelime.Ingilizce.contains(aramaKelime)){
                            kelime.Kelime_id = c.key!!
                            kelimelerListe.add(kelime)
                        }
                    }
                }
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }
}